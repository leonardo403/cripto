<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Cripto;
use Illuminate\Support\Facades\Crypt;

class Criptos extends Component
{
    public $id;
    public $nome;
    public $descricao;
    public $preco;
    public $criptos;
    public $isOpen = 0;

    public function render()
    {
        $this->criptos = Cripto::all();
        return view('livewire.criptos');
    }
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->nome = '';
        $this->descricao = '';
        $this->preco = '';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->validate([
            'nome' => 'required',
            'preco' => 'required',
        ]);

        Cripto::updateOrCreate(['id' => $this->id], [
            'nome' => $this->nome,
            'descricao' => $this->descricao,
            'preco'     => $this->preco
        ]);

        session()->flash('message',
            $this->id ? 'Cripto criada com sucesso.' : 'Cripo criada com sucesso.');

        $this->closeModal();
        $this->resetInputFields();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $cripto = Cripto::findOrFail($id);
        $this->id = $id;
        $this->nome = $cripto->nome;
        $this->descricao = $cripto->descricao;
        $this->preco     = $cripto->preco;

        $this->openModal();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Cripto::find($id)->delete();
        session()->flash('message', 'Cripo removida com sucesso.');
    }
}
